

weixinsdk plugin

`npm install --save @wmslei78/react-native-weixin`

`react-native link @wmslei78/react-native-weixin`


iOS

Define custom paths:

WX_SDK_DIR=

Add libWeChatSDK.a，WXApi.h，WXApiObject.h to your project.

Add SystemConfiguration.framework, libz.dylib, libsqlite3.0.dylib, libc++.dylib, Security.framework, CoreTelephony.framework, CFNetwork.framework to your project.

Library Search Path add libWeChatSDK.a dir.

URL type add “URL scheme” value is your appid.



Android

create a "wxapi" folder in your package path, and create a "WXEntryActivity" class which extends "com.wmslei78.rn.weixin.WXEntryActivity" in the "wxapi" folder.

add follow code in your AndroidManifest.xml file

```
<activity
  android:name=".wxapi.WXEntryActivity"
  android:exported="true"
  android:label="@string/app_name"
  android:theme="@android:style/Theme.Translucent.NoTitleBar"
  />
```



More Usage see sample: https://wmslei78@bitbucket.org/wmslei78/jointlogin-sample-rn


