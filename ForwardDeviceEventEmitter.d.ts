import { EmitterSubscription } from 'react-native';
export default class ForwardDeviceEventEmitter {
    private prefix;
    constructor(prefix: string);
    addListener(eventType: string, listener: (...args: any[]) => any, context?: any): EmitterSubscription;
    once(eventType: string, listener: (...args: any[]) => any, context: any): EmitterSubscription;
    removeAllListeners(eventType?: string): void;
    removeCurrentListener(): void;
    removeSubscription(subscription: EmitterSubscription): void;
    listeners(eventType: string): EmitterSubscription[];
    emit(eventType: string): void;
    removeListener(eventType: string, listener: (...args: any[]) => any): void;
}
