'use strict'
Object.defineProperty(exports, "__esModule", { value: true });

import { NativeModules, Linking, Platform, NativeEventEmitter } from 'react-native';
const RNWeixin = NativeModules.RNWeixin;

function register(appId) {
    Linking.addEventListener('url', handleOpenURL);
    return RNWeixin.register(appId);
}
exports.register = register;

function destroy() {
    Linking.removeEventListener('url', handleOpenURL);
    return RNWeixin.destroy();
}
exports.destroy = destroy;

function auth(scope, state) {
    return RNWeixin.auth(scope, state);
}
exports.auth = auth;

function handleOpenURL(event) {
    RNWeixin.handleOpenURL(event.url);
}

const ForwardDeviceEventEmitter_1 = require("./ForwardDeviceEventEmitter");
exports.WXEventEmitter = Platform.select({
    android: new ForwardDeviceEventEmitter_1.default('rn-weixin.'),
    ios: new NativeEventEmitter(RNWeixin),
});
