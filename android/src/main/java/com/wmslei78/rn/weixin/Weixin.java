package com.wmslei78.rn.weixin;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;

public class Weixin extends ReactContextBaseJavaModule implements IWXAPIEventHandler {

    private IWXAPI api;
    private String authState;

    private static Weixin weixin;

    public Weixin( ReactApplicationContext context ) {
        super( context );
        weixin = this;
    }

    @Override
    public String getName() {
        return "RNWeixin";
    }

    @ReactMethod
    public void register( String appId, final Promise promise ) {
        api = WXAPIFactory.createWXAPI(getCurrentActivity(), appId);
        if ( api.registerApp(appId) ) {
            promise.resolve( null );
        } else {
            promise.reject( "-1", "register" );
        }
    }

    @ReactMethod
    public void destroy( final Promise promise ) {
        if (api != null) {
            api.unregisterApp();

            promise.resolve( null );
        } else {
            promise.reject( "-1", "destroy" );
        }
    }

    @ReactMethod
    public void auth( String scope, String state, final Promise promise ) {
        if (api != null) {
            this.authState = state;

            final SendAuth.Req req = new SendAuth.Req();
            req.scope = scope;
            req.state = state;
            api.sendReq(req);

            promise.resolve( null );
        } else {
            promise.reject( "-1", "auth" );
        }
    }

    @ReactMethod
    public void handleOpenURL( String url ) {
    }

    public static void handleIntent(Intent intent) {
        if (weixin != null && weixin.api != null) {
            weixin.api.handleIntent(intent, weixin);
        }
    }

    @Override
    public void onReq(BaseReq baseReq) {}

    @Override
    public void onResp(BaseResp baseResp) {
        if (baseResp instanceof SendAuth.Resp) {
            onAuthResp((SendAuth.Resp) baseResp);
        }
    }

    private void onAuthResp(SendAuth.Resp resp) {
        if (resp.errCode == BaseResp.ErrCode.ERR_OK) {
            if (this.authState != null && this.authState.equals(resp.state)) {
                WritableMap params = Arguments.createMap();
                params.putString( "code", resp.code );
                sendEvent( "rn-weixin.authSuccess", params );
            } else {
                WritableMap params = Arguments.createMap();
                params.putInt( "code", -400 );
                params.putString( "description", "bad state" );
                sendEvent( "rn-weixin.error", params );
            }
        } else {
            WritableMap params = Arguments.createMap();
            params.putInt( "code", resp.errCode );
            params.putString( "description", resp.errStr );
            sendEvent( "rn-weixin.error", params );
        }
    }

    private void sendEvent( String eventName, @Nullable WritableMap params ) {
        getReactApplicationContext().
            getJSModule( DeviceEventManagerModule.RCTDeviceEventEmitter.class ).
            emit( eventName, params );
    }
}
