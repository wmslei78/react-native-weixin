"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
class ForwardDeviceEventEmitter {
    constructor(prefix) {
        this.prefix = prefix;
    }
    addListener(eventType, listener, context) {
        return react_native_1.DeviceEventEmitter.addListener(`${this.prefix}${eventType}`, listener, context);
    }
    once(eventType, listener, context) {
        return react_native_1.DeviceEventEmitter.once(`${this.prefix}${eventType}`, listener, context);
    }
    removeAllListeners(eventType) {
        react_native_1.DeviceEventEmitter.removeAllListeners(eventType && `${this.prefix}${eventType}`);
    }
    removeCurrentListener() {
        react_native_1.DeviceEventEmitter.removeCurrentListener();
    }
    removeSubscription(subscription) {
        react_native_1.DeviceEventEmitter.removeSubscription(subscription);
    }
    listeners(eventType) {
        return react_native_1.DeviceEventEmitter.listeners(`${this.prefix}${eventType}`);
    }
    emit(eventType) {
        react_native_1.DeviceEventEmitter.emit(`${this.prefix}${eventType}`);
    }
    removeListener(eventType, listener) {
        react_native_1.DeviceEventEmitter.removeListener(`${this.prefix}${eventType}`, listener);
    }
}
exports.default = ForwardDeviceEventEmitter;
