
export declare function register(appId: string): Promise<void>;
export declare function destroy(): Promise<void>;
export declare function auth(scope: 'snsapi_userinfo', state: string): Promise<void>;

export interface Error {
    code: number;
    description: string;
}

export interface AuthedEvent {
    code: string;
}

import { EventEmitter, EmitterSubscription } from 'react-native';
export { EmitterSubscription } from 'react-native';
declare module 'react-native' {
    interface DeviceEventEmitterStatic {
        addListener(type: 'rn-weixin.error', listener: (e: Error) => void, context?: any): EmitterSubscription;
        addListener(type: 'rn-weixin.authSuccess', listener: (result: AuthedEvent) => void, context?: any): EmitterSubscription;
    }
}
export interface WXEventEmitterStatic extends EventEmitter {
    addListener(type: 'error', listener: (e: Error) => void, context?: any): EmitterSubscription;
    addListener(type: 'authSuccess', listener: (result: AuthedEvent) => void, context?: any): EmitterSubscription;
}
export declare const WXEventEmitter: WXEventEmitterStatic;
