//
//  RNWeixin.h
//  RNWeixin
//
//  Created by sunlei on 2017/5/18.
//
//

#ifndef RNWeixin_h
#define RNWeixin_h

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RNWeixin : RCTEventEmitter <RCTBridgeModule>

@end

#endif /* RNWeixin_h */
