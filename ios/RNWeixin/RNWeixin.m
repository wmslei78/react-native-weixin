//
//  RNWeixin.m
//  RNWeixin
//
//  Created by sunlei on 2017/5/18.
//
//

#import "RNWeixin.h"
#import "WXApiManager.h"

@interface RNWeixin() {
    NSString *_authState;
}

@end

@implementation RNWeixin

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(register: (NSString *) appId
                  resolver: (RCTPromiseResolveBlock) resolve
                  rejecter: (RCTPromiseRejectBlock) reject)
{
    if ([WXApi registerApp:appId])
        resolve( nil );
    else
        reject( @"-1", @"failure", nil );
}


RCT_REMAP_METHOD(destroy,
                 destroyWithResolver: (RCTPromiseResolveBlock) resolve
                 rejecter: (RCTPromiseRejectBlock) reject)
{
    if (YES)
        resolve( nil );
    else
        reject( @"-1", @"destroy", nil );
}

RCT_EXPORT_METHOD(handleOpenURL: (NSString *) url)
{
    [WXApi handleOpenURL:[NSURL URLWithString: url] delegate:[WXApiManager sharedManager]];
}

RCT_EXPORT_METHOD(auth: (NSString *) scope
                  state: (NSString *) state
                  resolver: (RCTPromiseResolveBlock) resolve
                  rejecter: (RCTPromiseRejectBlock) reject)
{
    [WXApiManager sharedManager].delegate = self;
    _authState = [state copy];
    
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = scope;
    req.state = _authState;
    
    if ([WXApi sendAuthReq:req
            viewController:[self getRootViewController]
                  delegate:[WXApiManager sharedManager]])
        resolve( nil );
    else
        reject( @"-1", @"auth", nil );
}

- (UIViewController*) getRootViewController {
    UIViewController *root = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (root.presentedViewController != nil) {
        root = root.presentedViewController;
    }
    return root;
}


#pragma mark - RCTEventEmitter

- (NSArray<NSString *> *) supportedEvents {
    return @[ @"error",
              @"authSuccess",
              ];
}

#pragma mark - WXApiManager Delegate

- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    if (response.errCode == 0) {
        if ([response.state isEqualToString:_authState]) {
            [self sendEventWithName: @"authSuccess"
                               body: @{ @"code": response.code }];
        } else {
            [self sendEventWithName: @"error"
                               body: @{ @"code": [NSNumber numberWithInt: -400],
                                        @"description": @"bad state",
                                        }];
        }
    } else {
        [self sendEventWithName: @"error"
                           body: @{ @"code": [NSNumber numberWithInt: response.errCode],
                                    @"description": response.errStr,
                                    }];
    }
}

@end
